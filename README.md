# Bank OCR Kata
Following instructions from [here](https://github.com/testdouble/contributing-tests/wiki/Bank-OCR-kata)!

### Thank you for taking the time to review my coding exercise!

Total time spent: 3 hours, 15 minutes actively coding across a start-to-finish window of ~5 hours, with a lunch break and some dog walks throughout ([meet my dog Teddy here](https://www.instagram.com/teddy_thefloof_allen/)). This 3:15:00 tally includes time spent authoring this README so that you might gain further insight into how I think through a technical problem! That time also includes style linting, because I made the code work earlier, and now it's consistently indented more readable (without changing functionality). *I realize I went beyond the 3-hour window stipulated in my instructions - I missed a 'timeboxing' countdown alert on the home stretch, and went overtime -- My apologies on that.*

To run this code against a file of known-valid OCR-captured account numbers, git clone this repository to your local environment (Node 10.16.0 (lts/dubnium) was used) and execute the following the project directory. There are no external dependencies, so should work right out of the box!
```javascript
npm run parse ./examples/valid_ocr_accounts.txt

// or

yarn parse ./examples/valid_ocr_accounts.txt
```

To run the [Jasmine](https://jasmine.github.io/index.html) unit tests, there is one lightweight dependency (a more descriptive, behavior driven unit test suite that I prefer) to install. Using the package manager of your choice, install dependencies and run the unit test command:
```javascript
npm install
npm test

// or

yarn
yarn test
```

## Planning and Design Thoughts
3 hour time limit - scoping/time boxing, plan of attack === critical

### User Story 1
Knowing the following design criteria per file:
* "A normal file contains around 500 entries"
* "Each entry is 4 lines long, and each line has 27 characters. The first 3 lines of each entry contain an account number written using pipes and underscores, and the fourth line is blank."
* Each file's contents fit well within limits of JS string and array data structure.
* Unknown number of files, structure in such a way that each file in a potentially tremendous collection of files can be handled by a separate worker.
* File is created on an unknown platform - account for Windows `\r\n` vs Linux/Mac `\n` in the extract stage

"Your first task is to write a program that can take this file and parse it into actual account numbers."

Logical steps with the assumptions:
1. Define constants for known-good OCR string values mapped to the corresponding integer.
2. Synchronously load a file of known-good Account OCR captures as a string
3. Separate each 4-line known-good account OCR capture
4. Evaluate known-good account OCR capture to strings representing integer values
5. Evaluate each digit to corresponding integer values
6. Print account numbers to console upon completion of parsing for visual feedback.

## TODOS LATER?
### User Story 2
### User Story 3
### User Story 4