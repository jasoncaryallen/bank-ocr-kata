"use strict";

const fileLoader = require('./src/file_loader.js');
const parser = require('./src/parser.js');

function main() {
    let inputFilePath = process.argv.slice(2)[0];

    if (inputFilePath === undefined) {
        console.error('Target filepath is not defined');
        console.log('Usage: node <<PATH TO FILE>>');
        process.exit(1);
    } // Gracefully handle running this script without an input file provided
    
    console.log(`Now importing ${inputFilePath}`);
    let rawInputFileData = fileLoader.import(inputFilePath);

    console.log(`Now extracting account information from OCR captures in ${inputFilePath}`);
    const result = parser.extractAccounts(rawInputFileData);
    
    // Do something useful with the result and exit with code "0"
    console.log(result);
    process.exit(0);
};

main();