"use strict";

const { sanitize,
        divideLineDataByOcrAccountCapture,
        extractOcrAccountDigits,
        parseOcrDigits } = require('./../src/parser_utilities.js');

describe('Parser utilities', function() {
    describe('sanitize', function() {
        const sanitizedData = [
            "foo bar baz",
            "foo bar baz",
            "foo bar baz",
            ""
        ];

        it("replaces carriage returns with newline chars only, splits result on newline char", function() {
            const rawData = "foo bar baz\r\n" +
                                "foo bar baz\r\n" +
                                "foo bar baz\r\n";

            expect( sanitize(rawData) ).toEqual(sanitizedData);
        });

        it("Does not alter data containing newline chars only, splits result on newline char", function() {
            const rawData = "foo bar baz\n" +
                            "foo bar baz\n" +
                            "foo bar baz\n";

            expect( sanitize(rawData) ).toEqual(sanitizedData);
        });
    });

    describe('divideLineDataByOcrAccountCapture', function() {
        const lineData = [
            "foo",
            "bar",
            "baz",
            "tab",
            "foo1",
            "bar1",
            "baz1",
            "tab1"
        ]

        it("reorganizes the array of data lines into groupings by OCR account capture", function() {
            const expectedResult = [
                ["foo", "bar", "baz", "tab"],
                ["foo1", "bar1", "baz1", "tab1"]
            ];

            expect( divideLineDataByOcrAccountCapture(lineData) ).toEqual(expectedResult);
        });
    });

    describe('extractOcrAccountDigits', function() {
        it("parses a string representing an OCR account capture into the corresponding substrings representing digits", function() {
            const validOcrAccountCapture = [
                "    _  _     _  _  _  _  _ ",
                "  | _| _||_||_ |_   ||_||_|",
                "  ||_  _|  | _||_|  ||_| _|",
                ""
            ]

            const expectedExtractedDigits = [
                '     |  |', // 1
                ' _  _||_ ', // 2
                ' _  _| _|', // 3
                '   |_|  |', // 4
                ' _ |_  _|', // 5
                ' _ |_ |_|', // 6
                ' _   |  |', // 7
                ' _ |_||_|', // 8
                ' _ |_| _|'  // 9
            ]

            expect( extractOcrAccountDigits(validOcrAccountCapture) ).toEqual( expectedExtractedDigits );
        });
    });

    describe('parseOcrDigits', function() {
        it("parses an array of strings representing OCR characters to an array of corresponding integers", function() {
            const extractedDigits = [
                '     |  |', // 1
                ' _  _||_ ', // 2
                ' _  _| _|', // 3
                '   |_|  |', // 4
                ' _ |_  _|', // 5
                ' _ |_ |_|', // 6
                ' _   |  |', // 7
                ' _ |_||_|', // 8
                ' _ |_| _|'  // 9
            ]

            const expectedResult = [1, 2, 3, 4, 5, 6, 7, 8, 9];

            expect( parseOcrDigits(extractedDigits) ).toEqual( expectedResult );
        });
    });
});