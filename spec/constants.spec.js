"use strict";
const { ocrDigitValueLookup } = require('./../src/constants.js');

describe('Constants', function() {
    it('ocrDigitValueLookup() returns the correct integer when a valid OCR Character String', function() {
        let zero = " _ " +
                   "| |" +
                   "|_|";

        let one = "   " +
                  "  |" +
                  "  |";

        let two  = " _ " +
                   " _|" +
                   "|_ ";

        let three = " _ " +
                    " _|" +
                    " _|";

        let four = "   " +
                   "|_|" +
                   "  |";

        let five = " _ " +
                   "|_ " +
                   " _|";

        let six = " _ " +
                  "|_ " +
                  "|_|";

        let seven = " _ " +
                    "  |" +
                    "  |";

        let eight = " _ " +
                    "|_|" +
                    "|_|";

        let nine = " _ " +
                   "|_|" +
                   " _|";

        expect( ocrDigitValueLookup(zero) ).toEqual(0);
        expect( ocrDigitValueLookup(one) ).toEqual(1);
        expect( ocrDigitValueLookup(two) ).toEqual(2);
        expect( ocrDigitValueLookup(three) ).toEqual(3);
        expect( ocrDigitValueLookup(four) ).toEqual(4);
        expect( ocrDigitValueLookup(five) ).toEqual(5);
        expect( ocrDigitValueLookup(six) ).toEqual(6);
        expect( ocrDigitValueLookup(seven) ).toEqual(7);
        expect( ocrDigitValueLookup(eight) ).toEqual(8);
        expect( ocrDigitValueLookup(nine) ).toEqual(9);
    });
});
