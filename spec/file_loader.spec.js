"use strict";

const fileLoader = require('./../src/file_loader.js');

describe('File Loader', function() {
    it('can load a file that is present', function() {
        const importedFile = fileLoader.import('./examples/valid_ocr_accounts.txt');
        expect(importedFile.length).toBeGreaterThan(0);
        expect(importedFile).not.toBe(null);
        expect(importedFile).not.toBe(undefined);
    });

    it('throws an error when a file is not present', function() {
        expect( () => fileLoader.import('./impossible/missing_file.txt') ).toThrow();
    });
});
