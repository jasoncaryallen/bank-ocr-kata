"use strict";

const { extractAccounts } = require('./../src/parser.js');

describe('Parser', function() {
    describe('extractAccounts', function() {
        it("can parse a known-good multiline OCR file to an array of arrays representing accounts", function() {
            const rawData =
                " _     _  _     _  _  _  _ \r\n" +
                "| |  | _| _||_||_ |_   ||_|\r\n" +
                "|_|  ||_  _|  | _||_|  ||_|\r\n" +
                "\r\n" +
                " _  _  _  _  _  _  _  _  _ \r\n" +
                "|_||_||_||_||_||_||_||_||_|\r\n" +
                " _| _| _| _| _| _| _| _| _|\r\n" +
                "\r\n" +
                "    _  _     _  _  _  _  _ \r\n" +
                "  | _| _||_||_ |_   ||_||_|\r\n" +
                "  ||_  _|  | _||_|  ||_| _|\r\n" +
                "\r\n";

            const expectedResult = [
                [0, 1, 2, 3, 4, 5, 6, 7, 8],
                [9, 9, 9, 9, 9, 9, 9, 9, 9],
                [1, 2, 3, 4, 5, 6, 7, 8, 9]
            ]
            expect( extractAccounts(rawData) ).toEqual( expectedResult );
        });
    });
});
