"use strict";
const parserUtilities = require('./parser_utilities.js');
const constants = require( './constants.js' );

module.exports = {
    extractAccounts: function(rawInputFileData) {
        const sanitzedInputFileLineData = parserUtilities.sanitize( rawInputFileData );
        const ocrAccountCaptures = parserUtilities.divideLineDataByOcrAccountCapture( sanitzedInputFileLineData );
        let result = [];

        for (var ocrAccount of ocrAccountCaptures) {
            if (ocrAccount.length === constants.LINES_PER_OCR_ACCOUNT) {
                let extractedOcrDigitStrings = parserUtilities.extractOcrAccountDigits( ocrAccount );
                result.push( parserUtilities.parseOcrDigits( extractedOcrDigitStrings ));
            }
        }
        return result;
    }
}
