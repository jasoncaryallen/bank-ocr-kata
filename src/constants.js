"use strict";

const ZERO = " _ " +
             "| |" +
             "|_|";

const ONE = "   " +
            "  |" +
            "  |";

const TWO  = " _ " +
             " _|" +
             "|_ ";

const THREE = " _ " +
              " _|" +
              " _|";

const FOUR = "   " +
             "|_|" +
             "  |";

const FIVE = " _ " +
             "|_ " +
             " _|";

const SIX = " _ " +
            "|_ " +
            "|_|";

const SEVEN = " _ " +
              "  |" +
              "  |";

const EIGHT = " _ " +
              "|_|" +
              "|_|";

const NINE = " _ " +
             "|_|" +
             " _|";

const OCR_TO_DIGIT_MAPPING = {
    [ZERO]: 0,
    [ONE]: 1,
    [TWO]: 2,
    [THREE]: 3,
    [FOUR]: 4,
    [FIVE]: 5,
    [SIX]: 6,
    [SEVEN]: 7,
    [EIGHT]: 8,
    [NINE]: 9
};

const LINES_PER_OCR_ACCOUNT = 4;
const OCR_CHAR_WIDTH  = 3;

module.exports = {
    ocrDigitValueLookup: function(digitString) {
        // Assumes only valid digit strings for user story 1,
        // therefore no undefined results/bad input is accounted for here.
        return OCR_TO_DIGIT_MAPPING[digitString];
    },
    LINES_PER_OCR_ACCOUNT: LINES_PER_OCR_ACCOUNT,
    OCR_CHAR_WIDTH: OCR_CHAR_WIDTH
};
