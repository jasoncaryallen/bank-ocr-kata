"use strict";

const fs = require('fs');

module.exports = {
    import: function(inputFilePath) {
        // Synchronously load input file data
        //   This is on the assumption that this process will execute on files containing less than 1000
        //   OCR-Captured accounts, and on a queued-worker-per-file basis.
        //   Per the instructions, to expect ~500 OCR-Captured accounts per file, or ~2000 lines.
        //   JS readFile or readStream would offer more scalable solutions for larger files.
        
        // Gracefully handle issues with FileSystem accessing the input filepath and exit on error if so
        // Assign the file data to the buffer rawInputFileData
        return fs.readFileSync(inputFilePath, 'utf8', (err, data) => {
            if (err) throw err; 
            return data;
        });
    }
}
