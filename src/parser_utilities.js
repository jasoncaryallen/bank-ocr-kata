"use strict";

const constants = require('./constants.js');

module.exports = {
    sanitize: function(rawInputFileData) {
        return rawInputFileData.toString().replace(/\r\n/g,'\n').split('\n');
    },
    divideLineDataByOcrAccountCapture: function(sanitzedLineData) {
        sanitzedLineData
        let i, length, result = []
        for (i = 0, length = sanitzedLineData.length; i < length; i += constants.LINES_PER_OCR_ACCOUNT) {
            result.push( sanitzedLineData.slice(i, i + constants.LINES_PER_OCR_ACCOUNT) );
        }
        return result;
    },
    extractOcrAccountDigits: function(ocrAccountLines) {
        let result = [];
        let rows = ocrAccountLines.slice(0, -1);

            for (const row of rows) {
                for (let rowCharOffset = 0, resultIndex = 0;
                    rowCharOffset < row.length;
                    rowCharOffset += constants.OCR_CHAR_WIDTH, resultIndex += 1) {
                result[resultIndex] = result[resultIndex] || '';
                result[resultIndex] += row.slice(rowCharOffset, rowCharOffset + constants.OCR_CHAR_WIDTH);
                }
            }
        return result;
    },
    parseOcrDigits: function(extractedOcrDigitStrings) {
        return extractedOcrDigitStrings.map(digitString => constants.ocrDigitValueLookup(digitString));
    }
}
